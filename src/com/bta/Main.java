package com.bta;

public class Main {

    public static void main(String[] args) {
        // surasti visus skaicius nuo 1 iki 1000

        // Sukuriam skaiciu klases masyva
        Skaicius[] manoSkaiciai = new Skaicius[10001];

        // Sukuriame viena skaiciaus klases objekta
        Skaicius test = new Skaicius(12);
        System.out.println("As sukuriau nauja skaiciaus objekta:" + test.reiksme);

        for(int i = 1; i <= 10000; i++) {
            // Sukuriame viena skaiciaus klases objekta,bet idedame ji i skaiciu masyva
            manoSkaiciai[i] =  new Skaicius(i);
            if(manoSkaiciai[i].arTobulasis()) {
                System.out.println("Skaicius: " + manoSkaiciai[i].reiksme + " yra tobulasis skaicius");
            }
        }

    } // main funkcijos pabaiga
}
