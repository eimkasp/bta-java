package com.bta;

public class Skaicius {
    int reiksme;
    // Visu skaiciaus dalikliu masyvas
    int[] dalikliai = new int[1000];
    int dalikliuKiekis = 0;
    int dalikliuSuma = 0;

    Skaicius(int skaicius) {
        this.reiksme = skaicius;
        this.surastiDaliklius();
    }

    public void surastiDaliklius() {
        for(int i = 1; i < this.reiksme; i++) {
            // Tikrinu ar i reiksme yra skaiciaus daliklis
            if(this.reiksme % i == 0) {
                this.dalikliai[dalikliuKiekis] = i;
                this.dalikliuKiekis++;
            }
        }

        this.suskaiciuotiDalikliuSuma();
    }

    public void suskaiciuotiDalikliuSuma() {
        for(int i = 0; i < this.dalikliuKiekis; i++) {
            this.dalikliuSuma += this.dalikliai[i];
        }
    }

    public boolean arTobulasis() {
        if(this.reiksme == this.dalikliuSuma) {
            return true;
        } else {
            return false;
        }
    }

    public void spausdintiDaliklius() {
        for(int i = 0; i < this.dalikliuKiekis; i++) {
            System.out.println(this.dalikliai[i]);
        }
    }
}
